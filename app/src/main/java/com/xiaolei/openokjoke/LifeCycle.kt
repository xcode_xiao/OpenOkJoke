package com.xiaolei.openokjoke

import android.app.Activity
import android.app.Application
import android.os.Bundle

/**
 * Created by xiaolei on 2018/1/16.
 */
object LifeCycle : Application.ActivityLifecycleCallbacks
{
    override fun onActivityPaused(activity: Activity?)
    {

    }

    override fun onActivityResumed(activity: Activity?)
    {

    }

    override fun onActivityStarted(activity: Activity?)
    {

    }

    override fun onActivityDestroyed(activity: Activity)
    {
        ActivityStore.remove(activity)
    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?)
    {

    }

    override fun onActivityStopped(activity: Activity?)
    {

    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?)
    {
        ActivityStore.add(activity)
    }
}