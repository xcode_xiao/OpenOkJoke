package com.xiaolei.openokjoke.Cache

import android.content.Context
import android.graphics.Typeface


/**
 * Created by xiaolei on 2018/3/14.
 */
object FontCache
{
    public val fontCache = HashMap<String, Typeface>()
    fun initFont(context: Context)
    {
        val fontName = "lixunke.ttf"
        val typeFace = Typeface.createFromAsset(context.assets, fontName)
        typeFace?.let {
            FontCache.fontCache[fontName] = typeFace
        }
    }
}