package com.xiaolei.openokjoke

import android.app.Activity
import java.util.*

/**
 * Created by xiaolei on 2018/1/16.
 */
object ActivityStore
{
    private val activitys = LinkedList<Activity>()
    fun add(activity: Activity) = activitys.add(activity)
    fun remove(activity: Activity) = activitys.remove(activity)
    fun finishAll()
    {
        activitys.filter { activity ->
            !activity.isFinishing
        }.forEach { ac ->
            ac.finish()
        }
    }
    fun exit()
    {
        finishAll()
        android.os.Process.killProcess(android.os.Process.myPid())
    }
}