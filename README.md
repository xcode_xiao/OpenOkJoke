# OpenOkJoke
开源一款自己闲余时间用Kotlin开发的看笑话的笑话APP

这款APP当时的目的是为了逗自己开心，不过最近心态很好，索性就开源出来算了。

服务端是用的Kotlin Spring+SpringMVC+MyBatis，另外，**请各位大佬不要攻击我服务器谢谢**。

有些图标是我自己P的，有些是自己做的，更多的是偷的。

[![image](https://gitee.com/xcode_xiao/OpenOkJoke/raw/master/app/src/main/assets/a.jpg)](https://my.oschina.net/xiaolei123/blog)

---------------------------------------------------

[![image](https://gitee.com/xcode_xiao/OpenOkJoke/raw/master/app/src/main/assets/b.jpg)](https://my.oschina.net/xiaolei123/blog)

---------------------------------------------------

[![image](https://gitee.com/xcode_xiao/OpenOkJoke/raw/master/app/src/main/assets/c.jpg)](https://my.oschina.net/xiaolei123/blog)

---------------------------------------------------

[编译好的APK下载地址](https://gitee.com/xcode_xiao/OpenOkJoke/raw/master/app/src/main/assets/app-release.apk)


